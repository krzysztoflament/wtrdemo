<?php

use App\Kernel;
use Spiral\RoadRunner\PSR7Client;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;
use Spiral\RoadRunner\Worker;
use Spiral\Goridge\SocketRelay;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Zend\Diactoros\ResponseFactory;
use Zend\Diactoros\ServerRequestFactory;
use Zend\Diactoros\StreamFactory;
use Zend\Diactoros\UploadedFileFactory;
use Symfony\Component\HttpFoundation\Cookie;

require dirname(__DIR__).'/config/bootstrap.php';

if ($_SERVER['APP_DEBUG']) {
    umask(0000);

    Debug::enable();
}

if ($trustedProxies = $_SERVER['TRUSTED_PROXIES'] ?? $_ENV['TRUSTED_PROXIES'] ?? false) {
    Request::setTrustedProxies(explode(',', $trustedProxies), Request::HEADER_X_FORWARDED_ALL ^ Request::HEADER_X_FORWARDED_HOST);
}

if ($trustedHosts = $_SERVER['TRUSTED_HOSTS'] ?? $_ENV['TRUSTED_HOSTS'] ?? false) {
    Request::setTrustedHosts([$trustedHosts]);
}

$kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);

$worker = new Worker(
    new SocketRelay('localhost', 7000)
);
$psr7 = new PSR7Client($worker);
$httpFoundationFactory = new HttpFoundationFactory();
$psrHttpFactory = new PsrHttpFactory(
    new ServerRequestFactory(),
    new StreamFactory(),
    new UploadedFileFactory(),
    new ResponseFactory()
);

while ($request = $psr7->acceptRequest()) {
    try {
        $request = $httpFoundationFactory->createRequest($request);

        if ($request->cookies->has(session_name())) {
            session_id($request->cookies->get(session_name()));
        }

        $response = $kernel->handle($request);

        $cookieOptions = $kernel->getContainer()->getParameter('session.storage.options');

        $response->headers->setCookie(
            new Cookie(
                session_name(),
                session_id(),
                $cookieOptions['cookie_lifetime'] ?? 0,
                $cookieOptions['cookie_path'] ?? '/',
                $cookieOptions['cookie_domain'] ?? '',
                ($cookieOptions['cookie_secure'] ?? 'auto') === 'auto' ? $request->isSecure(
                ) : (bool)($cookieOptions['cookie_secure'] ?? 'auto'),
                $cookieOptions['cookie_httponly'] ?? true,
                false,
                $cookieOptions['cookie_samesite'] ?? null
            )
        );

        $psr7->respond($psrHttpFactory->createResponse($response));
        $kernel->terminate($request, $response);
        $kernel->reboot(null);
    } catch (Throwable $e) {
        $psr7->getWorker()->error((string)$e);
    } finally {
        if (PHP_SESSION_ACTIVE === session_status()) {
            session_write_close();
        }
        session_id('');
        session_unset();
        unset($_SESSION);
    }
}
