#!/usr/bin/make -f

mutagen-start:
	@mutagen create --name=wtrdemo $$(pwd) docker://wtrdemo_roadrunner_1/app --symlink-mode ignore
		--ignore-vcs -m two-way-resolved --default-owner-beta www-data --default-group-beta www-data

mutagen-stop:
	@mutagen terminate -a
	@mutagen daemon stop
